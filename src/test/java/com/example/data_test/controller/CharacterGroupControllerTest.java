package com.example.data_test.controller;

import com.example.data_test.contact.CharacterGroupRequest;
import com.example.data_test.domin.CharacterGroup;
import com.example.data_test.repository.CharacterGroupRepository;
import com.example.data_test.service.CharacterGroupService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.awt.*;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringRunner.class)

class CharacterGroupControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CharacterGroupRepository repository;

    @BeforeEach
    void setUp() throws JsonProcessingException {

    }

    @Test
    void getAllGroups() {

    }

    @Test
    void should_add_new_record_when_send_post_request() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        CharacterGroup characterGroup = new CharacterGroup("group_5", "power off");
        String groupAsString = mapper.writeValueAsString(characterGroup);
        int formerSize = repository.findAll().size();
        mockMvc.perform(post("/character_group").content(groupAsString).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());
        int latterSize = repository.findAll().size();
        assertEquals(formerSize+1, latterSize);
    }

    @Test
    void should_update_character_group_when_send_update_request() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        CharacterGroupRequest characterGroupRequest = new CharacterGroupRequest("group_5", "power off",2,100);
        String requestAsString = mapper.writeValueAsString(characterGroupRequest);
        CharacterGroup oldGroup = repository.findById("111").get();
        assertNotEquals("group_5",oldGroup.getGroupName());
        assertNotEquals("power off",oldGroup.getDescription());
        mockMvc.perform(patch("/character_group/111").content(requestAsString).contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8));
        CharacterGroup newGroup = repository.findById("111").get();
        assertEquals("group_5",newGroup.getGroupName());
        assertEquals("power off",newGroup.getDescription());

    }

    @Test
    void shu() {
    }
}