CREATE TABLE IF NOT EXISTS character_group (
    group_id VARCHAR(64),
    group_name VARCHAR(200) NOT NULL,
    description VARCHAR(4000),
    priority INT NOT NULL default 1,
    time_window INT NOT NULL default 300,
    primary key (group_id)
);

CREATE TABLE IF NOT EXISTS ops_event (
    rule_id VARCHAR(64),
    rule_name VARCHAR(200) NOT NULL,
    event_category VARCHAR(64) NOT NULL default 'TroubleEvent',
    event_number VARCHAR(64) NOT NULL,
    vendor VARCHAR(200),
    domain VARCHAR(200),
    clear_rule INT NOT NULL,
    is_correlation INT NOT NULL default 1,
    description VARCHAR(4000),
    time_window INT NOT NULL default 300,
    generate_function VARCHAR(200),
    script_name VARCHAR(200),
    group_id VARCHAR(64),
    primary key (rule_id),
    foreign key (group_id) REFERENCES character_group (group_id)
        on update restrict on delete cascade
);

