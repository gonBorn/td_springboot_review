package com.example.data_test.domin;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class OpsEvent {
    @Id
    @GeneratedValue(generator = "foo-uuid")
    @GenericGenerator(name = "foo-uuid", strategy = "uuid2")
    private String ruleId;
    @NotNull
    private String ruleName;
    @NotNull
    private String eventCategory;
    @NotNull
    private String eventNumber;
    private String vendor;
    private String domain;
    @NotNull
    private Integer clearRule;
    @NotNull
    private Integer isCorrelation;
    private String description;
    @NotNull
    private Integer timeWindow;
    private String generateFunction;
    private String scriptName;
    @ManyToOne
    @JoinColumn(name="group_id")
    private CharacterGroup characterGroup;
}
