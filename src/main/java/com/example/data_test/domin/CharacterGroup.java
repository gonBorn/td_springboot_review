package com.example.data_test.domin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class CharacterGroup {
    @Id
    @GeneratedValue(generator = "foo-uuid")
    @GenericGenerator(name = "foo-uuid", strategy = "uuid2")
    private String groupId;

    @Column(nullable = false)
    private String groupName;

    private String description;

    //@Column(columnDefinition = "int default 1")
    private Integer priority;

    //@Column(columnDefinition = "int default 300")
    private Integer timeWindow;

    public CharacterGroup(String groupName, String description) {
        this.groupName = groupName;
        this.description = description;
    }
}
