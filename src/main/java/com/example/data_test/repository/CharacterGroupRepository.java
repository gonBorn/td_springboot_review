package com.example.data_test.repository;

import com.example.data_test.domin.CharacterGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterGroupRepository extends JpaRepository<CharacterGroup, String> {
}
