package com.example.data_test.repository;

import com.example.data_test.domin.OpsEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.awt.*;

@Repository
public interface OpsEventRepository extends JpaRepository<OpsEvent, String> {

}
