package com.example.data_test.service;

import com.example.data_test.domin.OpsEvent;
import com.example.data_test.repository.OpsEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OpsEventService {
    @Autowired
    private OpsEventRepository eventRepository;

    public List<OpsEvent> getAllEvents() {
        return eventRepository.findAll();
    }
}
