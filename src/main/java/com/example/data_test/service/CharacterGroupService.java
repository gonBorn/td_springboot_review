package com.example.data_test.service;

import com.example.data_test.contact.CharacterGroupRequest;
import com.example.data_test.domin.CharacterGroup;
import com.example.data_test.domin.OpsEvent;
import com.example.data_test.repository.CharacterGroupRepository;
import com.example.data_test.repository.OpsEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class CharacterGroupService {
    @Autowired
    private CharacterGroupRepository groupRepository;

    public List<CharacterGroup> getAllEvents() {
        return groupRepository.findAll();
    }

    public String saveCharacterGroup(CharacterGroup group) {
        return groupRepository.save(group).getGroupId();
    }

    public CharacterGroup updateCharacterGroup(CharacterGroupRequest request, String groupId) {
        CharacterGroup characterGroup = groupRepository.findById(groupId).orElseThrow(() -> new NoSuchElementException());
        characterGroup.setGroupName(request.getGroupName());
        characterGroup.setDescription(request.getDescription());
        characterGroup.setPriority(request.getPriority());
        characterGroup.setTimeWindow(request.getTimeWindow());
        return groupRepository.save(characterGroup);
    }

    public void deleteCharacterGroup(String groupId) {
        CharacterGroup characterGroup = groupRepository.findById(groupId).orElseThrow(() -> new NoSuchElementException());
        groupRepository.delete(characterGroup);
    }
}
