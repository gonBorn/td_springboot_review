package com.example.data_test.contact;

import lombok.Data;

import javax.persistence.Column;

@Data
public class CharacterGroupRequest {
    private String groupName;
    private String description;
    private Integer priority;
    private Integer timeWindow;

    public CharacterGroupRequest(String groupName, String description, Integer priority, Integer timeWindow) {
        this.groupName = groupName;
        this.description = description;
        this.priority = priority;
        this.timeWindow = timeWindow;
    }
}
