package com.example.data_test.controller;

import com.example.data_test.domin.OpsEvent;
import com.example.data_test.service.OpsEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/ops_event")
public class OpsEventController {

    @Autowired
    private OpsEventService eventService;

    @GetMapping("/...")
    public ResponseEntity<List<OpsEvent>> getAllEvents() {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(eventService.getAllEvents());
    }
}
