package com.example.data_test.controller;

import com.example.data_test.contact.CharacterGroupRequest;
import com.example.data_test.domin.CharacterGroup;
import com.example.data_test.service.CharacterGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/character_group")
public class CharacterGroupController {
    @Autowired
    private CharacterGroupService groupService;
    @GetMapping
    public ResponseEntity<List<CharacterGroup>> getAllGroups() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(groupService.getAllEvents());
    }

    @PostMapping
    public ResponseEntity<String> createCharacterGroup(@RequestBody CharacterGroup group) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(groupService.saveCharacterGroup(group));
    }

    @PutMapping("/{groupId}")
    public ResponseEntity<CharacterGroup> updateCharacterGroup(@RequestBody CharacterGroupRequest request, @PathVariable String groupId) {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(groupService.updateCharacterGroup(request,groupId));
    }

    @DeleteMapping("/{groupId}")
    public ResponseEntity deleteCharacterGroup(@PathVariable String groupId) {
        groupService.deleteCharacterGroup(groupId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
